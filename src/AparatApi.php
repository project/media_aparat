<?php

namespace Drupal\media_aparat;

use Drupal\Core\Logger\LoggerChannel;
use Drupal\Component\Serialization\Json;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use GuzzleHttp\ClientInterface as HttpClientInterface;
use GuzzleHttp\Exception\GuzzleException;

/**
 * Class AparatApi
 */
class AparatApi {
  use StringTranslationTrait;

  /**
   * @var \Drupal\Core\Logger\LoggerChannel
   */
  protected LoggerChannel $logger;

  protected HttpClientInterface $httpClient;

  /**
   * AparatApi constructor.
   *
   * @param \Drupal\Core\Logger\LoggerChannel $logger
   * @param \GuzzleHttp\ClientInterface $httpClient
   */
  public function __construct(LoggerChannel $logger, HttpClientInterface $httpClient) {
    $this->logger = $logger;
    $this->httpClient = $httpClient;
  }

  /**
   * get Video info from video hash
   *
   * @param string $hash
   * The vide hash
   *
   * @return array|bool
   */
  public function getVideoInfo(string $hash) {
    $result = FALSE;

    try {
      $response = $this->httpClient->request('GET', "https://www.aparat.com/etc/api/video/videohash/" . $hash);
      if ($response->getStatusCode() == 200) {
        $response_json = $response->getBody()->getContents();
        $response_data = Json::decode($response_json);

        if (isset($response_data['video'])) {
          $result = $response_data['video'];
        }
      }
    }
    catch (GuzzleException $e) {
      $this->logger->error('GuzzleException when calling aparat API.');
    }

    return $result;
  }

  /**
   * extract video hash from aparat view URL
   *
   * @param string $url The View url of the aparat video
   *
   * @return string|bool
   * The video hash, false on error
   */
  public function extractVideoHashFromUrl(string $url) {
    $matches = [];
    $hash = FALSE;
    $pattern = '#^https?://(www\.)?aparat\.com/v/([a-zA-Z0-9]+).*$#';

    if (preg_match($pattern, $url, $matches)) {
      $hash = $matches[2];
    }
    else {
      $this->logger->warning('Invalid Aparat video url: @url', ['@url' => $url]);
    }

    return $hash;
  }
}
