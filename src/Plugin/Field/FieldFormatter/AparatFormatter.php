<?php

namespace Drupal\media_aparat\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\Annotation\FieldFormatter;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\media\Entity\MediaType;
use Drupal\media_aparat\AparatApi;
use Drupal\media_aparat\Plugin\media\Source\AparatVideo;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'aparat' formatter.
 *
 * @FieldFormatter(
 *   id = "aparat",
 *   label = @Translation("Aparat"),
 *   field_types = {
 *     "link",
 *     "string",
 *     "string_long"
 *   }
 * )
 */
class AparatFormatter extends FormatterBase implements ContainerFactoryPluginInterface {

  /**
   * @var \Drupal\media_aparat\AparatApi
   */
  protected AparatApi $aparat_api;

  /**
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected LoggerChannelInterface $logger;

  /**
   * Constructs a FormatterBase object.
   *
   * @param string $plugin_id
   *   The plugin_id for the formatter.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the formatter is associated.
   * @param array $settings
   *   The formatter settings.
   * @param string $label
   *   The formatter label display setting.
   * @param string $view_mode
   *   The view mode.
   * @param array $third_party_settings
   *   Any third party settings.
   * @param \Drupal\media_aparat\AparatApi $aparat_api
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   */
  public function __construct($plugin_id,
                              $plugin_definition,
                              FieldDefinitionInterface $field_definition,
                              array $settings,
                              $label,
                              $view_mode,
                              array $third_party_settings,
                              AparatApi $aparat_api,
                              LoggerChannelFactoryInterface $logger_factory) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings);
    $this->aparat_api = $aparat_api;
    $this->logger = $logger_factory->get('media_aparat');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('media_aparat.api'),
      $container->get('logger.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    foreach ($items as $delta => $item) {
      $main_property = $item->getFieldDefinition()->getFieldStorageDefinition()->getMainPropertyName();
      $value = $item->{$main_property};

      if (empty($value)) {
        continue;
      }

      $hash = $this->aparat_api->extractVideoHashFromUrl($value);
      if (empty($hash)) {
        continue;
      }

      $metadata = $this->aparat_api->getVideoInfo($hash);

      if (empty($metadata)) {
        $this->logger->error('Cannot fetch aparat metadata for URL: @url', ['@url' => $value]);
        $this->messenger()->addWarning('Cannot fetch aparat metadata.');
        continue;
      }

      $elements[$delta] = [
        '#type' => 'html_tag',
        '#tag' => 'iframe',
        '#attributes' => [
          'src' => $metadata['frame'],
          'title' => $metadata['title'],
          'frameborder' => 0,
          'scrolling' => FALSE,
          'allowtransparency' => TRUE,
          //'width' => $max_width ?: $resource->getWidth(),
          //'height' => $max_height ?: $resource->getHeight(),
          'class' => ['media-aparat-content'],
        ],
      ];
    }

    return $elements;
  }

  /**
   * Prevent usage of this field formatter on any fields other than those which are added in
   * a media type which it's source is AparatVideo
   */
  public static function isApplicable(FieldDefinitionInterface $field_definition) {
    if ($field_definition->getTargetEntityTypeId() !== 'media') {
      return FALSE;
    }

    if (parent::isApplicable($field_definition)) {
      $media_type = $field_definition->getTargetBundle();

      if ($media_type) {
        $media_type = MediaType::load($media_type);
        return $media_type && $media_type->getSource() instanceof AparatVideo;
      }
    }
    return FALSE;
  }
}
