<?php

namespace Drupal\media_aparat\Plugin\Field\FieldWidget;

use Drupal\Core\Field\Annotation\FieldWidget;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldWidget\StringTextfieldWidget;
use Drupal\Core\Form\FormStateInterface;
use Drupal\media\Entity\MediaType;
use Drupal\media_aparat\Plugin\media\Source\AparatVideo;

/**
 * Plugin implementation of the 'aparat_textfield' widget.
 *
 * @FieldWidget(
 *   id = "aparat_textfield",
 *   label = @Translation("Aparat URL"),
 *   field_types = {
 *     "string",
 *   },
 * )
 */
class AparatWidget extends StringTextfieldWidget {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element = parent::formElement($items, $delta, $element, $form, $form_state);

    $message = $this->t('Please enter a valid aparat video URL.');

    if (!empty($element['#value']['#description'])) {
      $element['value']['#description'] = [
        '#theme' => 'item_list',
        '#items' => [$element['value']['#description'], $message],
      ];
    }
    else {
      $element['value']['#description'] = $message;
    }

    return $element;
  }

  /**
   * Prevent usage of this field widget on any fields other than those which are added in
   * a media type which it's source is AparatVideo
   */
  public static function isApplicable(FieldDefinitionInterface $field_definition) {
    $target_bundle = $field_definition->getTargetBundle();

    if (!parent::isApplicable($field_definition) || $field_definition->getTargetEntityTypeId() !== 'media' || !$target_bundle) {
      return FALSE;
    }

    return MediaType::load($target_bundle)->getSource() instanceof AparatVideo;
  }
}
