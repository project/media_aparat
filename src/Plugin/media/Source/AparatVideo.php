<?php

namespace Drupal\media_aparat\Plugin\media\Source;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\Display\EntityFormDisplayInterface;
use Drupal\Core\Entity\Display\EntityViewDisplayInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\FieldTypePluginManagerInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\media\Annotation\MediaSource;
use Drupal\media\MediaInterface;
use Drupal\media\MediaSourceBase;
use Drupal\media\MediaTypeInterface;
use Drupal\media_aparat\AparatApi;
use GuzzleHttp\ClientInterface as HttpClientInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class AparatVideo
 *
 * @MediaSource(
 *   id = "aparat_video",
 *   label = @Translation("Aparat Video"),
 *   description = @Translation("Use videos from aparat.com"),
 *   allowed_field_types = {"string"},
 *   default_thumbnail_filename = "no-thumbnail.png",
 *   forms = {
 *     "media_library_add" = "\Drupal\media_aparat\Form\AddVideoForm",
 *   },
 * )
 */
class AparatVideo extends MediaSourceBase {

  /**
   * The logger channel for media.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * The messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The HTTP client.
   *
   * @var \GuzzleHttp\Client
   */
  protected $httpClient;

  /**
   * The Aparat Api
   */
  protected AparatApi $aparatApi;

  /**
   *
   */
  protected $fileSystem;

  /**
   * Constructs a new OEmbed instance.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory service.
   * @param \Drupal\Core\Field\FieldTypePluginManagerInterface $field_type_manager
   *   The field type plugin manager service.
   * @param \Psr\Log\LoggerInterface $logger
   *   The logger channel for media.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   * @param \GuzzleHttp\ClientInterface $http_client
   *   The HTTP client.
   * @param \Drupal\media_aparat\AparatApi $aparatApi
   *  The aparat api
   * @param \Drupal\Core\File\FileSystemInterface|null $file_system
   *   The file system.
   */
  public function __construct(array $configuration,
                              $plugin_id,
                              $plugin_definition,
                              EntityTypeManagerInterface $entity_type_manager,
                              EntityFieldManagerInterface $entity_field_manager,
                              ConfigFactoryInterface $config_factory,
                              FieldTypePluginManagerInterface $field_type_manager,
                              LoggerInterface $logger,
                              MessengerInterface $messenger,
                              HttpClientInterface $http_client,
                              AparatApi $aparatApi,
                              FileSystemInterface $file_system = NULL) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $entity_type_manager, $entity_field_manager, $field_type_manager, $config_factory);
    $this->logger = $logger;
    $this->messenger = $messenger;
    $this->httpClient = $http_client;
    $this->aparatApi = $aparatApi;

    if (!$file_system) {
      @trigger_error('The file_system service must be passed to OEmbed::__construct(), it is required before Drupal 9.0.0. See https://www.drupal.org/node/3006851.', E_USER_DEPRECATED);
      $file_system = \Drupal::service('file_system');
    }
    $this->fileSystem = $file_system;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('entity_field.manager'),
      $container->get('config.factory'),
      $container->get('plugin.manager.field.field_type'),
      $container->get('logger.factory')->get('media_aparat'),
      $container->get('messenger'),
      $container->get('http_client'),
      $container->get('media_aparat.api'),
      $container->get('file_system')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getMetadataAttributes() {
    return [
      'title'        => $this->t('Video title'),
      'author_id'    => $this->t('Author ID'),
      'author_name'  => $this->t('Author name'),
      'big_poster'   => $this->t('Big poster'),
      'small_poster' => $this->t('Small poster'),
      'duration'     => $this->t('Video duration'),
      'sdate'        => $this->t('Video post date'),
      'iframe'       => $this->t('Iframe src'),
      'description'  => $this->t('Video description'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getMetadata(MediaInterface $media, $name) {
    $media_url = $this->getSourceFieldValue($media);
    // The URL may be NULL if the source field is empty, in which case just
    // return NULL.
    if (empty($media_url)) {
      return NULL;
    }

    $aparat_hash = $this->aparatApi->extractVideoHashFromUrl($media_url);
    if (empty($aparat_hash)) {
      $this->messenger->addError($this->t('Invalid Aparat Video Url.'));
      return NULL;
    }

    $metadata = $this->aparatApi->getVideoInfo($aparat_hash);

    if (empty($metadata)) {
      $this->messenger->addError($this->t('Cannot fetch video metadata from aparat service.'));
      return NULL;
    }

    switch ($name) {
      case 'default_name':
        if ($title = $this->getMetadata($media, 'title')) {
          return $title;
        }
        elseif ($url = $this->getMetadata($media, 'iframe')) {
          return $url;
        }
        return parent::getMetadata($media, 'default_name');

      case 'title':
        return $metadata['title'];

      case 'author_id':
        return $metadata['username'];

      case 'author_name':
        return $metadata['sender_name'];

      case 'big_poster':
        return $metadata['big_poster'];

      case 'small_poster':
        return $metadata['small_poster'];

      case 'duration':
        return $metadata['duration'];

      case 'sdate':
        return $metadata['sdate'];

      case 'iframe':
        return $metadata['frame'];

      case 'description':
        return $metadata['description'];

      case 'thumbnail_uri':
        return $metadata['big_poster'];

      default:
        return $metadata[$name] ?? parent::getMetadata($media, $name);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function prepareViewDisplay(MediaTypeInterface $type, EntityViewDisplayInterface $display) {
    $display->setComponent($this->getSourceFieldDefinition($type)->getName(), [
      'type' => 'aparat',
      'label' => 'visually_hidden',
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function prepareFormDisplay(MediaTypeInterface $type, EntityFormDisplayInterface $display) {
    parent::prepareFormDisplay($type, $display);
    $source_field = $this->getSourceFieldDefinition($type)->getName();

    $display->setComponent($source_field, [
      'type' => 'aparat_textfield',
      'weight' => $display->getComponent($source_field)['weight'],
    ]);
    $display->removeComponent('name');
  }

  /**
   * {@inheritdoc}
   */
  public function createSourceField(MediaTypeInterface $type) {
    $plugin_definition = $this->getPluginDefinition();

    $label = (string) $this->t('@type URL', [
      '@type' => $plugin_definition['label'],
    ]);
    return parent::createSourceField($type)->set('label', $label);
  }
}
